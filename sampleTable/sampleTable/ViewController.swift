//
//  ViewController.swift
//  sampleTable
//
//  Created by Sabari on 1/24/17.
//  Copyright © 2016 aktrea. All rights reserved.
//

import UIKit




class ViewController: UIViewController,UITableViewDelegate {
    var arr:[String]=["aaa","ssss","fff","ggg","hhh"]
    override func viewDidLoad() {
        
//     self.main.registerClass(TableViewCell.self, forCellReuseIdentifier: "cell")
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBOutlet weak var main: UITableView!
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr.count
        
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath)->UITableViewCell
    {
     let cell=self.main.dequeueReusableCellWithIdentifier("cell")as! TableViewCell
        
        
        
        cell.lbl?.text=self.arr[indexPath.row]
        return cell
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("touched");
    }
}
