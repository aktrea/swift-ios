//
//  ViewController.swift
//  Signalr Swift
//
//  Created by Sabari on 11/1/17.
//  Copyright © 2017 aktrea. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet var userName: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var loginButton: UIButton!
    
//    var hubConnection = SRHubConnection(urlString: "http://aktrea.com/fintech");
//    var chat = SRHubProxy()
    var chat: Hub!
    var hubConnection: SignalR!

    @IBAction func loginButton(_ sender: Any) {
        self.view.endEditing(true)
        self.view.frame.origin.y = 0
        loginData()
        
        
        // Hubs...
//        hubConnection = SignalR("http://aktrea.com/fintech")
//        hubConnection.useWKWebView = true
//        hubConnection.transport = .serverSentEvents
//        chat = Hub("chatHub")
//        
//        chat.on("SendNotificationToGroup")

        
        
       
       // chat = SRHubProxy(connection: hubConnection, hubName: "chatHub");
        
       
     //  chat.on("SendNotificationToGroup", perform: self, selector: #selector(ViewController.signInResponse(response:response1:)))
        
        
//        chat.on("SendNotificationToGroup")
//        { args in
//            let message = args![0] as! String
//            let detail = args![1] as! String
//            print("Message: \(message)\nDetail: \(detail)")
//        }
        
//        hubConnection?.started = { [weak self] in
//            print("Starting...")
//            self?.chat.invoke("Connect", withArgs:nil)
//        }
//        hubConnection?.received = { [weak self] received in
//            print("Error: \(received)")
//            
//        }
//        
//        hubConnection?.reconnecting = { [weak self] in
//            print("Reconnecting...")
//        }
//        
//        hubConnection?.connectionSlow = { print("Connection slow...") }
//        
//        hubConnection?.error = { [weak self] error in
//            print("Error: \(error)")
//        }

        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.delegate=self
        password.delegate=self
        
        
    }
    // Do any additional setup after loading the view, typically from a nib.
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*!
     *@brief While enter username and password keyboardDidShow fuction execute(NSNotification).
     */
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.autocorrectionType = .no
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardDidShow), name:NSNotification.Name.UIKeyboardDidShow, object: nil);
        return true
        
    }
    /*!
     *@brief keyboard disappear when click return/done button
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    /*!
     *@brief after enter username and password keyboardDidHide fuction execute(NSNotification).
     */
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.autocorrectionType = .no
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardDidHide), name:NSNotification.Name.UIKeyboardDidHide, object: nil);
        return true
        
        
    }
    /*!
     *@brief hide statusbar
     */
    override var prefersStatusBarHidden: Bool {
        return true
    }
    /*!
     *@brief move view to top when keyboard appears
     */
    func keyboardDidShow(notification:NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if(self.view.frame.origin.y == 0)
            {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    /*!
     *@brief set view to orginal height when keyboard hide
     */
    func keyboardDidHide(notification:NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if(self.view.frame.origin.y != 0)
            {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
        
    }
    /*!
     *@brief login using URLSession pass username and password get response from server save login userid in UserDefaults
     */
    func loginData()
    {
        let urlValue=URL(string: "http://aktrea.com/fintech/api/LoginApi/")!
        var request=URLRequest(url: urlValue)
        let session=URLSession.shared
        request.httpMethod="POST"
        let paramString="Email=\(userName.text!)&Password=\(password.text!)"
        print(paramString)
        request.httpBody=paramString.data(using: String.Encoding.utf8)
        let task = session.dataTask(with: request)
        {
            (data, response, error) in
            guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil
                else
            {
                print("error")
                return
            }
            //remove '"' from string
            var dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            dataString = dataString?.replacingOccurrences(of: "\"", with: "") as NSString?
            //seperate string to get userid
            let userValue=dataString?.components(separatedBy: "_")
            //save userid in UserDefaults
            let userId = UserDefaults.standard
            userId.set(userValue![1], forKey: "userid")
            userId.synchronize()
            DispatchQueue.main.async(){
                self.performSegue(withIdentifier: "loginSuccess", sender:nil)
            }
            
            
        }
        task.resume()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "loginSuccess")
        {
            DispatchQueue.main.async(){
                _ = segue.destination as! UINavigationController
            }
        }
    }
    func signalr()
    {
        
                self.hubConnection.start()
//        chat.on("SendNotificationToGroup", perform: self, selector: #selector(ViewController.signInResponse(response:response1:)))
    }
    func signInResponse(response: String, response1: String) {
        
        print(response);
    }
}
