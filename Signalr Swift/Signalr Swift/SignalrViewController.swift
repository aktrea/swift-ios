//
//  SignalrViewController.swift
//  Pods
//
//  Created by Sabari on 12/1/17.
//
//

import UIKit

class SignalrViewController: UIViewController {
    var chat: Hub!
    var hubConnection: SignalR!
    override func viewDidLoad() {
        super.viewDidLoad()
        hubConnection = SignalR("http://aktrea.com/fintech")
        hubConnection.useWKWebView = false
        hubConnection.signalRVersion = .v2_2_0
        chat = Hub("chatHub")
        
        chat.on("SendNotificationToGroup")
        { args in
            let message = args![0] as! NSDictionary
            let detail = args![1] as! NSNumber
            print("Message: \(message)\nDetail: \(detail)")
        }
        hubConnection.addHub(chat)
        // SignalR events
        
        hubConnection.starting = { [weak self] in
            print("Starting...")
        }
        
        hubConnection.reconnecting = { [weak self] in
            print("Reconnecting...")
            
        }
        
        hubConnection.connected = { [weak self] in
            print("Connected. Connection ID: \(self?.hubConnection.connectionID!)")
             self!.chat.invoke("Connect", arguments: nil)
        }
        
        hubConnection.reconnected = { [weak self] in
            print("Reconnected. Connection ID: \(self?.hubConnection.connectionID!)")
            
        }
        
        hubConnection.disconnected = { [weak self] in
            print("Disconnected.")
        }
        
        hubConnection.connectionSlow = { print("Connection slow...") }
        
        hubConnection.error = { [weak self] error in
            print("Error: \(error)")
            
            // Here's an example of how to automatically reconnect after a timeout.
            //
            // For example, on the device, if the app is in the background long enough
            // for the SignalR connection to time out, you'll get disconnected/error
            // notifications when the app becomes active again.
            
            if let source = error?["source"] as? String , source == "TimeoutException" {
                print("Connection timed out. Restarting...")
                self?.hubConnection.start()
            }
        }
        
        hubConnection.start()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
