//
//  ChatViewController.swift
//  Chat
//
//  Created by Sabari on 23/1/17.
//  Copyright © 2017 aktrea. All rights reserved.
//

import UIKit
//display chat based on user selection.
//if user select groupname, group chat will be diaplay
//if user select username, private caht will be dispaly
class ChatViewController: UIViewController,UITableViewDataSource{
    
    static var GROUP_MESSAGE : Int = 0
    static var PRIVATE_MESSAGE : Int = 1
    var groupName = String()
    var groupId = Int()
    var session = URLSession.shared
    var message :[AnyObject] = []
    var toUserId = NSNumber()
    var userName = String()
    var userConnectionId = String()
    var messageStatus = Int()
    var chatMessage = [String()]
    
    
    @IBOutlet var chatText: UITextField!
    
    @IBAction func sendMessage(_ sender: Any) {
        if(messageStatus == ChatViewController.GROUP_MESSAGE)
        {
            let currentUser = UserDefaults.standard.object(forKey: "currentUser") as! NSString
            let userId = UserDefaults.standard.object(forKey: "userid")
            let groupid = "\(self.groupId)"
            self.chatMessage.append(currentUser as String)
            self.chatMessage.append(self.chatText.text!)
            self.chatMessage.append(userId as! String)
            self.chatMessage.append(groupid)
            self.chatMessage.append(self.groupName)
            UserDefaults.standard.set(self.chatMessage, forKey: "currentGroupChatMessage")
            NotificationCenter.default.post(name: Notification.Name("sendGroupMessage"), object: nil)
            self.chatMessage.removeAll()
            self.chatText.text=""
        }
        else if(messageStatus == ChatViewController.PRIVATE_MESSAGE)
        {
            print(userConnectionId)
            print(chatText.text!)
            self.chatMessage.append(userConnectionId)
            self.chatMessage.append(chatText.text!)
            UserDefaults.standard.set(self.chatMessage, forKey: "currentChatMessage")
            NotificationCenter.default.post(name: Notification.Name("sendMessage"), object: nil)
            self.chatMessage.removeAll()
            self.chatText.text=""
        }
        //onlinecht.chat.invoke("SendPrivateMessage", arguments: [userConnectionId, chatText.text!])
    }
    @IBOutlet var chatTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.chatMessage.removeFirst()
        
        //get notification from groupmessage
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(receiveGroupMessage), name: NSNotification.Name(rawValue: "receiveGroupMessage"), object: nil)
        
        //get notification from usermessage
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(receiveUserMessage), name: NSNotification.Name(rawValue: "receiveUserMessage"), object: nil)
        
        //call getgroupmessage and getusermessage based od user selection
        if(messageStatus == ChatViewController.GROUP_MESSAGE)
        {
            self.getGroupMessage()
        }
        else
        {
            self.getUserMessage()
        }
        chatTable.backgroundColor? = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        print(userName)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    //get group mesage by passing groupname and groupid
    //display message in chat tableview
    func getGroupMessage()
    {
        let urlValue = URL(string: "http://aktrea.com/fintech/api/chat/getGroup/\(self.groupId)")!
        let task = session.dataTask(with: urlValue, completionHandler: {
            (data, response, error)in
            if error != nil{
                print(error!)
            }
            else
            {
                do
                {
                    self.message = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSMutableArray as [AnyObject]
                    print(self.message)
                    DispatchQueue.main.async(){
                        self.chatTable.reloadData()
                    }
                    
                }
                catch let error as NSError
                {
                    print(error)
                }
                
            }
        })
        task.resume()
        
    }
    
    //get user mesage by passing groupname and groupid
    //display message in chat tableview
    func getUserMessage()
    {
        let userId = UserDefaults.standard.object(forKey: "userid") as! NSString
        let urlValue = URL(string: "http://aktrea.com/fintech/api/chat/getChat/\(userId)/\(self.toUserId)/20")!
        let task = session.dataTask(with: urlValue, completionHandler: {
            (data, response, error)in
            if error != nil{
                print(error!)
            }
            else
            {
                do
                {
                    self.message = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSMutableArray as [AnyObject]
                    print(self.message)
                    DispatchQueue.main.async(){
                        self.chatTable.reloadData()
                    }
                    
                }
                catch let error as NSError
                {
                    print(error)
                }
                
            }
        })
        task.resume()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.message.count
    }
    //display group message details based on current login user name
    //display private message details baesd on current login user name.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let senderCell=self.chatTable.dequeueReusableCell(withIdentifier: "SenderCell")!as! SenderTableViewCell
        let reciverCell=self.chatTable.dequeueReusableCell(withIdentifier: "ReciverCell")!as! ReciverTableViewCell
        senderCell.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        reciverCell.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        
        let currentUserName = UserDefaults.standard.object(forKey: "currentUser") as! NSString
        
        
        //display groupmessage and user message bassed on message status.
        //if message status 0 means group message will display
        //if message status 1 means group message will display
        if messageStatus == ChatViewController.GROUP_MESSAGE
        {
            let postedBy = (self.message[indexPath.row] as AnyObject).value(forKey: "PostedBy") as! NSString
            if (postedBy == currentUserName)
            {
                senderCell.senderMessage.text = (self.message[indexPath.row] as AnyObject).value(forKey: "Message") as? String
                senderCell.senderName.text = (self.message[indexPath.row] as AnyObject).value(forKey: "PostedBy") as? String
                senderCell.senderTime.text = (self.message[indexPath.row] as AnyObject).value(forKey: "PostedAt") as? String
                return senderCell
            }
            else
            {
                reciverCell.receiverMessage.text = (self.message[indexPath.row] as AnyObject).value(forKey: "Message") as? String
                reciverCell.receiverName.text = (self.message[indexPath.row] as AnyObject).value(forKey: "PostedBy") as? String
                reciverCell.receiverTime.text = (self.message[indexPath.row] as AnyObject).value(forKey: "PostedAt") as? String
                return reciverCell
            }
            
            
        }
        else
        {
            let postedBy = (self.message[indexPath.row] as AnyObject).value(forKey: "frm") as! NSString
            if (postedBy as String != self.userName)
            {
                senderCell.senderMessage.text = (self.message[indexPath.row] as AnyObject).value(forKey: "message") as? String
                senderCell.senderName.text = (self.message[indexPath.row] as AnyObject).value(forKey: "frm") as? String
                senderCell.senderTime.text = (self.message[indexPath.row] as AnyObject).value(forKey: "PostedAt") as? String
                return senderCell
            }
            else
            {
                reciverCell.receiverMessage.text = (self.message[indexPath.row] as AnyObject).value(forKey: "message") as? String
                reciverCell.receiverName.text = (self.message[indexPath.row] as AnyObject).value(forKey: "frm") as? String
                reciverCell.receiverTime.text = (self.message[indexPath.row] as AnyObject).value(forKey: "PostedAt") as? String
                return reciverCell
            }
            
            
        }
        
        
        
        
    }
    //update group message from signalr responce when user message to any group
    func updateGroupMessage(userName: String, groupMessage: String, group_Id: NSNumber, group_Name: String, date: String)
    {
        let groupMessages : [String:Any] = ["ID" :group_Id, "Message":groupMessage, "PostedAt":date, "PostedBy":userName]
        UserDefaults.standard.set(groupMessages, forKey: "groupMessage")
        NotificationCenter.default.post(name: Notification.Name("receiveGroupMessage"),object: nil)
        
        
    }
    //update private message from signalr responce when user message to any user
    func updateMessage(connection_Id: AnyObject, userName: String, userMessage: String, touserName: String, date: String, fullName: String, toId: AnyObject)
    {
        //@"Name",@"PostedAt",@"frm",@"message",@"to"
        let groupMessages : [String:Any] = ["Name" :fullName, "PostedAt":date, "frm":userName, "message":userMessage, "to":touserName]
        UserDefaults.standard.set(groupMessages, forKey: "userMessage")
        NotificationCenter.default.post(name: Notification.Name("receiveUserMessage"),object: nil)
        
        
    }

    //get group message from notification and display 
    func receiveGroupMessage()
    {
        let receiveGroupMsg = UserDefaults.standard.object(forKey: "groupMessage")
        self.message.append(receiveGroupMsg! as AnyObject)
        self.chatTable.reloadData()
    }
    
    //get user message from notification and display
    func receiveUserMessage()
    {
        let receiveGroupMsg = UserDefaults.standard.object(forKey: "userMessage")
        self.message.append(receiveGroupMsg! as AnyObject)
        self.chatTable.reloadData()
    }

}

