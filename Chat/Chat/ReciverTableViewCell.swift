//
//  ReciverTableViewCell.swift
//  Chat
//
//  Created by Sabari on 23/1/17.
//  Copyright © 2017 aktrea. All rights reserved.
//

import UIKit

class ReciverTableViewCell: UITableViewCell {

    @IBOutlet var receiverName: UILabel!
    @IBOutlet var receiverTime: UILabel!
    @IBOutlet var receiverMessage: UILabel!
    @IBOutlet var receiverImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
