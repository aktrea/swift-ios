//
//  OnlineViewController.swift
//  Chat
//
//  Created by Sabari on 21/1/17.
//  Copyright © 2017 aktrea. All rights reserved.
//

import UIKit


//This view displays group names and online users names in a tableview
class OnlineViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    static var USER_SECTION_AND_GROUP_SECTION : Int = 2
    static var SECTION_GROUP : Int = 0
    static var SECTION_USER : Int = 1
    var chat : Hub!
    var hubConnection : SignalR!
    
    var onlineGroupnName = [String()]
    var onlineGroupId = [Int()]
    var onlineToUserId = [NSNumber()]
    var onlineUserName = [String()]
    var onlineuserConnectionId = [String()]
    var selectRow = Int()
    var selectSection = Int()
    
    @IBOutlet var onlineUserTable: UITableView!
    
    //when the view controller is first loaded into memory this method will execute
    // We will connect to the server, the signalr hub first.
    // Fetch the groups and users list using teh connection
    //
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        hubConnection = SignalR("https://aktrea.com/fintech")
        hubConnection.useWKWebView = false
        hubConnection.signalRVersion = .v2_2_0
        
        //receivce notification from ChatViewController when user send message to user
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(sendMessageToServer), name: NSNotification.Name(rawValue: "sendMessage"), object: nil)
        
        
        //receivce nstification from ChatViewController when user send message to group
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(sendGroupMessageToServer), name: NSNotification.Name(rawValue: "sendGroupMessage"), object: nil)
        
        //singalr communication is done using a hub
        chat = Hub("chatHub")
        hubConnection.addHub(chat)
        
        
        
        //get connected user list(Username,Connectionid,User) from signalR display Username in tableview
        chat.on("SendConnectedUsersList")
        {
            args in
            let message = args![0] as! NSArray
            print("Message: \(message)")
            for index in 0..<message.count
            {
                let userId = (message[index] as AnyObject).value(forKey: "UserId")!
                let userName = (message[index] as AnyObject).value(forKey: "UserName")!
                let connectionId = (message[index] as AnyObject).value(forKey: "ConnectionId")!
                self.onlineToUserId.append(userId as! NSNumber)
                self.onlineUserName.append(userName as! String)
                self.onlineuserConnectionId.append(connectionId as! String)
                self.onlineUserTable.reloadData()
                
            }
        }
        
        //get groupname(Groupname,Groupid) from signalR display Groupname in tableview
        chat.on("Group")
        {
            args in
            let groupID = args![0] as! NSNumber
            let groupName = args![1] as! NSString
            self.onlineGroupnName.append(groupName as String)
            self.onlineGroupId.append(groupID as Int)
            self.onlineUserTable.reloadData()
        }
        
        //get updateUser details when user login(Username,Connectionid,User) from signalR update Username in tableview
        chat.on("updateUser")
        {
            args in
            let message = args![0] as! NSDictionary
            print("Message: \(message)")
            let userId = message.value(forKey: "UserId")!
            let userName = message.value(forKey: "UserName")!
            let connectionId = message.value(forKey: "ConnectionId")!
            self.onlineToUserId.append(userId as! NSNumber)
            self.onlineUserName.append(userName as! String)
            self.onlineuserConnectionId.append(connectionId as! String)
            self.onlineUserTable.reloadData()
        }
        
        //update the userlist when user disconnect(onUserDisconnected).
        chat.on("onUserDisconnected")
        {
            args in
            let userId = args![2] as! NSNumber
            let userName = args![1] as! NSString
            let connectionId = args![0] as! NSString
            
            self.onlineUserName = self.onlineUserName.filter{ $0 != userName as String}
            self.onlineuserConnectionId = self.onlineuserConnectionId.filter{ $0 != connectionId as String}
            self.onlineToUserId = self.onlineToUserId.filter{$0 != userId}
            self.onlineUserTable.reloadData()
            
        }
        //get current login user name from signalr response and store in userdefaults
        chat.on("setDetails")
        {
            args in
            let userFullName = args![1] as! NSString
            print(userFullName)
            UserDefaults.standard.set(userFullName, forKey: "currentUser")
            UserDefaults.standard.synchronize()
        }
        
        //-(void)GroupMessage:(NSString *)uname Message:(NSString *)groupMessage groupid:(NSString*)Groupid Gname:(NSString *)gname DateHelper:(NSString *)date


        //get message from server and update message in ChatViewcontroller by calling updateMessage
        chat.on("sendPrivateMessage")
        {
            args in
            let connectionId = args![0] as AnyObject
            let userName = args![1] as! NSString
            let message = args![2] as! NSString
            let toUserName = args![3] as! NSString
            let date = args![4] as! NSString
            let userFullName = args![5] as! NSString
            let toId = args![6] as AnyObject
            
             ChatViewController().updateMessage(connection_Id: connectionId, userName: userName as String, userMessage: message as String, touserName: toUserName as String, date: date as String, fullName: userFullName as String, toId: toId)
            
            
        }
        
        //get group message from server and update message in ChatViewcontroller by calling updateGroupMessage
        chat.on("GroupMessage")
        {
            args in
            
            let userName = args![0] as! String
            let message = args![1] as! String
            let groupId = args![2] as! NSNumber
            let groupName = args![3] as! String
            let date = args![4] as! String
           
            ChatViewController().updateGroupMessage(userName: userName , groupMessage: message , group_Id: groupId, group_Name: groupName, date: date)
            
        }


        // SignalR events
        
        hubConnection.starting = { [weak self] in
            print("Starting...")
            
            
        }
        
        //invoke server method after connection start.
        hubConnection.connected = { [weak self] in
            print("Connected. Connection ID: \(self?.hubConnection.connectionID!)")
            self!.chat.invoke("Connect", arguments: nil)
            self!.chat.invoke("GetOnlineUser", arguments:nil)
            
        }
        
        //connection will recreate when disconnent or error
        hubConnection.reconnected = { [weak self] in
            print("Reconnected. Connection ID: \(self?.hubConnection.connectionID!)")
            
        }
        
        //show if connection Disconnected.
        hubConnection.disconnected = { [weak self] in
            print("Disconnected.")
        }
        
        //show if connection is slow.
        hubConnection.connectionSlow = { print("Connection slow...") }
        
        hubConnection.error = { [weak self] error in
            print("Error: \(error)")
            
            //  automatically reconnect after a timeout.
            // on the device, if the app is in the background long enough
            // for the SignalR connection to time out, you'll get disconnected/error
            
            if let source = error?["source"] as? String , source == "TimeoutException"
            {
                print("Connection timed out. Restarting...")
                self?.hubConnection.start()
            }
            
        }
        hubConnection.start()
    }
    
    //send Private message to server
    func sendMessageToServer()
    {
        let message = UserDefaults.standard.object(forKey: "currentChatMessage") as! NSArray
        print(message)
        self.chat.invoke("SendPrivateMessage", arguments:message as? [Any])
    }
    
    //send Group message to server
    func sendGroupMessageToServer()
    {
        let message = UserDefaults.standard.object(forKey: "currentGroupChatMessage") as! NSArray
        print(message)
        self.chat.invoke("sendServerMessageForGroups", arguments:message as? [Any])
    }

    // Return the number of sections we currently
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return OnlineViewController.USER_SECTION_AND_GROUP_SECTION
    }
    
    //set section color and text size in section view
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: "Futura", size: 20)!
        header.textLabel?.textColor = UIColor.red
    }
    
    //set tittle for section
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        if section == OnlineViewController.SECTION_GROUP
        {
            return "Group"
        }
        else if section == OnlineViewController.SECTION_USER
        {
            return "User"
        }
        else
        {
            return nil
        }
        
    }
    
    //set number of user and group length based on signalr response.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section==0
        {
            return onlineGroupId.count;
        }
        else
        {
            return onlineUserName.count
        }
    }
    
    //display groupname in section 0 and user name in section 1
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell()
        if indexPath.section == OnlineViewController.SECTION_GROUP
        {
            cell.textLabel?.text=onlineGroupnName[indexPath.row]
        }
        else if indexPath.section == OnlineViewController.SECTION_USER
        {
            cell.textLabel?.text=onlineUserName[indexPath.row]
        }
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectRow = indexPath.row
        selectSection = indexPath.section
        self.performSegue(withIdentifier: "chatview", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "chatview"
        {
            let chatView = segue.destination as! ChatViewController
            if selectSection == OnlineViewController.SECTION_GROUP
            {
                chatView.groupId = onlineGroupId[selectRow]
                chatView.groupName = onlineGroupnName[selectRow]
                chatView.messageStatus = selectSection
            }
            else if selectSection == OnlineViewController.SECTION_USER
            {
                chatView.toUserId = onlineToUserId[selectRow]
                chatView.userName = onlineUserName[selectRow]
                chatView.userConnectionId = onlineuserConnectionId[selectRow]
                chatView.messageStatus = selectSection
                
            }
        }
    }
}
