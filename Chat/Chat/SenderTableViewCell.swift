//
//  SenderTableViewCell.swift
//  Chat
//
//  Created by Sabari on 23/1/17.
//  Copyright © 2017 aktrea. All rights reserved.
//

import UIKit

class SenderTableViewCell: UITableViewCell {

    @IBOutlet var senderImage: UIImageView!
   
    @IBOutlet var senderName: UILabel!
    @IBOutlet var senderTime: UILabel!
    @IBOutlet var senderMessage: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
