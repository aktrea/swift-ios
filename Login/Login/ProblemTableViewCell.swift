//
//  ProblemTableViewCell.swift
//  Login
//
//  Created by Sabari on 10/1/17.
//  Copyright © 2017 aktrea. All rights reserved.
//

import UIKit

class ProblemTableViewCell: UITableViewCell {
    @IBOutlet var userNameText: UILabel!
    @IBOutlet var solutionCountText: UILabel!
    @IBOutlet var problemText: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
