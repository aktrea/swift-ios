//
//  ProblemViewController.swift
//  Login
//
//  Created by Sabari on 10/1/17.
//  Copyright © 2017 aktrea. All rights reserved.
//

import UIKit

class ProblemViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var jsonData=NSArray()
    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate=self
        tableView.dataSource=self
        getData()
        // Do any additional setup after loading the view.
    }
    /*!
     *@brief get problem data from server and reload the tableview to update the data
     */
    func getData()
    {
        let urlValue = URL(string: "http://aktrea.com/fintech/api/social/GetProblemandSolution/0/20")!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print("error")
                }
                else
                {
                    do
                    {
                        self.jsonData = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                        DispatchQueue.main.async(){
                            self.tableView.reloadData()
                        }
                    }
                    catch let error as NSError
                    {
                        print(error)
                    }
                }
        })
        task.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    /*!
     *@brief update number of post in tableview
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jsonData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellidentifier = "cell"
        
        //display problemtext in tableview cell from json
        let cell:ProblemTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: cellidentifier) as! ProblemTableViewCell
        let problem = jsonData.value(forKey: "Problemandsolution") as! NSArray
        let problemText = problem.value(forKey: "ProblemText") as! NSArray
        cell.problemText?.text=problemText[indexPath.row] as? String
        
        //display solution count in tableview cell from json
        let solution = problem.value(forKey: "Solution") as! NSArray
        let solutionCount=solution[indexPath.row] as! NSArray
        cell.solutionCountText?.text=String(solutionCount.count)
        
        //display username in tableview cell from json
        let postData = jsonData.value(forKey: "Posts") as! NSArray
        let userName = postData .value(forKey: "PostBy") as! NSArray
        cell.userNameText?.text=userName[indexPath.row] as? String
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //postProblem()
        postUsingUrl()
    }
    /*!
     *@brief post problem using json data. send jsondata in post as NSData
     */
    func postProblem()
    {
        //posted json data
        let postData = ["GroupId":32 , "PostContent":"Post problem using Swift 3" , "GeographicId":""] as [String : Any]
        print(postData)
        
        //convert jsondata to data
        let jsonData = try? JSONSerialization.data(withJSONObject: postData, options: .prettyPrinted)
        let urlValue = URL(string: "http://aktrea.com/fintech//api/Problem/PostProblemForGroup")!
        var request = URLRequest(url: urlValue)
        request.httpMethod="POST"
        request.httpBody=jsonData
        //set content type
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print(error!)
                }
                else
                {
                    do
                    {
                        //get response from server as dictionary
                        let problemPostResponse = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                        print(problemPostResponse! .value(forKey: "Problemandsolution")!)
                    }
                    
                }
                
        })
        
        task.resume()
    }
    /*!
     *@brief post method(post data with url)
     */
    func postUsingUrl()
    {
        let urlValue = URL(string: "http://aktrea.com/fintech/api/post/savelike?postId=2835")!
        var request = URLRequest(url: urlValue)
        request.httpMethod="POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler:
            {
                (data , response , error )in
                if(error != nil)
                {
                    print(error!)
                }
                else
                {
                    do
                    {
                        //get response from server as bool type(true/false)
                        let likePostResponse = (NSString(data: data!, encoding: String.Encoding.utf8.rawValue) != nil) as Bool
                        print(likePostResponse)
                    }
                }
        })
        task.resume()
    }
}

