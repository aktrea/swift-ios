//
//  ViewController.swift
//  Swift WebView
//
//  Created by Sabari on 11/1/17.
//  Copyright © 2017 aktrea. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UIWebViewDelegate {
    
    var userName = String()
    var password = String()
    var savedUserName = String()
    var savedPassword = String()
    @IBOutlet var webView: UIWebView!
    @IBOutlet var loaderImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.delegate=self
        //get saved username and password from userdefaults
        savedUserName=UserDefaults.standard.string(forKey: "userName") ?? ""
        savedPassword=UserDefaults.standard.string(forKey: "password") ?? ""
        //add images in imgListArray
        var imgListArray: [UIImage] = []
        for countValue in 1...7
        {
            let strImageName : String = "aktrea-loader-\(countValue).tiff"
            let image  = UIImage(named:strImageName)
            imgListArray.append(image!)
        }
        loaderImage.animationImages=imgListArray
        loaderImage.animationRepeatCount=0
        loaderImage.animationDuration=1;
        loaderImage.isHidden=true
        // call loadWebView() when app start loading
        loadWebView()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*!
     *@brief hide statusbar
     */
    override var prefersStatusBarHidden: Bool {
        return true
    }
    func loadWebView()
    {
        // load url content within webview
        let urlValue = URL(string: "http://aktrea.com/fintech")!
        let request = URLRequest(url: urlValue)
        self.webView.loadRequest(request)
    }
    /*!
     *@brief get username and password when user enter details
     */
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if(userName.characters.count == 0)
        {
            userName = self.webView.stringByEvaluatingJavaScript(from: "document.getElementById('LoginPage_Email').value")!
            password = self.webView.stringByEvaluatingJavaScript(from: "document.getElementById('LoginPage_Password').value")!
        }
        return true
    }
    /*!
     *@brief save user details after webview load and save into userdefaults
     */
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        let currentUrl = self.webView.stringByEvaluatingJavaScript(from: "window.location.href")
        if(userName.characters.count != 0) && (password.characters.count != 0)
        {
            if currentUrl == "http://aktrea.com/fintech/" {
                let javaScriptPass0 = "document.getElementById('social_logout').onclick();"
                self.webView.stringByEvaluatingJavaScript(from: javaScriptPass0)
                UserDefaults.standard.set(userName, forKey: "userName")
                UserDefaults.standard.set(password, forKey: "password")
                UserDefaults.standard.synchronize()
                print("saved Successfully")
            }
            else
            {
                userName=""
                password=""
            }
        }
        
        if savedUserName.characters.count != 0 && savedPassword.characters.count != 0{
            loaderImage.isHidden=false
            loaderImage.startAnimating()
            self.webView.isHidden=true
            let passUsername = "document.getElementById('LoginPage_Email').value='\(savedUserName)';"
            let passPassword = "document.getElementById('LoginPage_Password').value='\(savedPassword)';"
            let clickLogin = "document.getElementById('submit_login').click();"
            self.webView.stringByEvaluatingJavaScript(from: passUsername)
            self.webView.stringByEvaluatingJavaScript(from: passPassword)
            self.webView.stringByEvaluatingJavaScript(from: clickLogin)
            self.perform(#selector(hideLoader), with: loaderImage, afterDelay: 5.0)
            
            if(currentUrl == "")
            {
                let tempUser = UserDefaults.standard
                tempUser.removeObject(forKey: "userName")
                tempUser.removeObject(forKey: "password")
                tempUser.synchronize()
                savedUserName = tempUser.value(forKey: "userName") as! String
                savedPassword = tempUser.value(forKey: "password") as! String
                userName=""
                password=""
            }
            
        }
        
    }
    func hideLoader(loadImage : UIImageView)
    {
        self.webView.isHidden=false
        loaderImage.isHidden=true
        
    }
}

